package com.javaclass.anima.android20bradsignature

import android.graphics.Paint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    internal lateinit var myPaintView: PaintView

    private var clear: View? = null
    private var undo: View? = null
    private var redo: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        myPaintView = findViewById<View>(R.id.pview) as PaintView

        clear = findViewById(R.id.clear)
        undo = findViewById(R.id.undo)
        redo = findViewById(R.id.redo)


        clear!!.setOnClickListener { myPaintView.clearDraw() }

        undo!!.setOnClickListener { myPaintView.undoDraw() }


        redo!!.setOnClickListener { myPaintView.redoDraw() }
    }
}
