package com.javaclass.anima.android20bradsignature

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceView
import android.view.View

import java.util.HashMap
import java.util.LinkedList

/**
 * Created by anima on 2016/7/30.
 */
class PaintView(context: Context, attrs: AttributeSet) : View(context, attrs) {

    internal var paintLine: Paint
    internal var line = LinkedList<HashMap<String, Float>>()

    internal var lines = LinkedList<LinkedList<HashMap<String, Float>>>()
    internal var recyle = LinkedList<LinkedList<HashMap<String, Float>>>()

    private var istouched: Boolean = false


    init {

        // 設定畫筆
        paintLine = Paint()
        paintLine.color = Color.YELLOW
        paintLine.strokeWidth = 4f

        // 初始建構線條資料結構物件
        lines = LinkedList()
        recyle = LinkedList()

    }

    override fun onTouchEvent(event: MotionEvent): Boolean {

        // 手指離開螢幕
        if (event.action == MotionEvent.ACTION_UP) {
            istouched = false
        } else {
            val line: LinkedList<HashMap<String, Float>>

            if (!istouched) {
                // 觸摸開始
                line = LinkedList()
                lines.add(line)
                istouched = true
            } else {
                // 開始滑動
                line = lines.last
            }

            val point = HashMap<String, Float>()
            point["x"] = event.x
            point["y"] = event.y
            line.add(point)
            postInvalidate()
        }
        return true
    }

    // 清除畫面
    internal fun clearDraw() {
        lines.clear()
        postInvalidate()
    }

    // Undo功能
    internal fun undoDraw() {
        if (lines.size > 0) {
            recyle.add(lines.removeLast())
            postInvalidate()
        }
    }

    // Redo功能
    internal fun redoDraw() {
        if (recyle.size > 0) {
            lines.add(recyle.removeLast())
            postInvalidate()
        }
    }

    override fun onDraw(canvas: Canvas) {
        for (line in lines) {
            if (line.size > 1) {
                // 至少有兩點才需要畫出線段
                for (i in 1 until line.size) { // i = 1表示從第二點開始處理
                    val point1 = line[i - 1] // 前一點
                    val point2 = line[i] // 目前點
                    canvas.drawLine(point1["x"]!!, point1["y"]!!,
                            point2["x"]!!, point2["y"]!!, paintLine)
                }
            }
        }
    }
}
